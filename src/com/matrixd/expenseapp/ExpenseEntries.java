package com.matrixd.expenseapp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ExpenseEntries{
	static class Entry {
		private float s;
		private String desc;
		private int cat, d, acc, idNumber;
		
		public Entry(){}
		
		public Entry(int id){
			idNumber = id;
		}
		
		public void setSum(float sum){
			s = sum;
		}
		public void setDescription(String description){
			desc = description;
		}
		public void setCategory(int category){
			cat = category;
		}
		public void setDate(Date date){
			d = (int) date.getTime()/60000;
		}
		public void setDate(int date){
			d = date;
		}
		public void setAccount(int accountId){
			acc = accountId;
		}
		
		public int getDate(){
			return d;
		}
		public int getAccount(){
			return acc;
		}
		public int getCategory(){
			return cat;
		}
		public String getDescription(){
			return desc;
		}
		public float getSum(){
			return s;
		}
		public int getId(){
			return idNumber;
		}
	}
	
	private SQLiteDatabase db;
	
	
	public ExpenseEntries(SQLiteDatabase database){
		db = database;
	}
	
	public List<Entry> getEntries(int category, int account){
		String selection = null, orderBy = null, lim = null;
		List<ExpenseEntries.Entry> entries = new ArrayList<ExpenseEntries.Entry>();
		
		if(account>-1) selection = ExpenseDbSchema.ExpenseEntries.COLUMN_ACCOUNT + " = " + account;
		if(category>-1) selection = ExpenseDbSchema.ExpenseEntries.COLUMN_CAT + " = " + category;
		orderBy = ExpenseDbSchema.ExpenseEntries.COLUMN_DATE;
		//if(limit>0) lim = limit;
		
		Cursor res = db.query(
				ExpenseDbSchema.ExpenseBank.TABLE_NAME,
				null,
				selection,
				null,
				null,
				null,
				orderBy,
				lim
		);
		
		int idColumn = res.getColumnIndex(ExpenseDbSchema.ExpenseEntries.TABLES_ENTRY_ID);
		int descColumn = res.getColumnIndex(ExpenseDbSchema.ExpenseEntries.COLUMN_DESC);
		int catColumn = res.getColumnIndex(ExpenseDbSchema.ExpenseEntries.COLUMN_CAT);
		int dateColumn = res.getColumnIndex(ExpenseDbSchema.ExpenseEntries.COLUMN_DATE);
		int sumColumn = res.getColumnIndex(ExpenseDbSchema.ExpenseEntries.COLUMN_SUM);
		int accColumn = res.getColumnIndex(ExpenseDbSchema.ExpenseEntries.COLUMN_ACCOUNT);
		
		for(int i = 0; i<res.getCount(); i++){
			res.moveToPosition(i);
			Entry entry = new Entry(res.getInt(idColumn));
			entry.setDescription(res.getString(descColumn));
			entry.setCategory(res.getInt(catColumn));
			entry.setDate(res.getInt(dateColumn));
			entry.setSum(res.getFloat(sumColumn));
			entry.setAccount(res.getInt(accColumn));
			entries.add(entry);
		}
		return entries;
	}
	
	public void insert(ExpenseEntries.Entry entry){
		ContentValues values = new ContentValues();
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_ACCOUNT, entry.getAccount());
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_CAT, entry.getCategory());
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_DATE, entry.getDate());
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_DESC, entry.getDescription());
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_SUM, entry.getSum());
		
		db.insert(ExpenseDbSchema.ExpenseEntries.TABLE_NAME, null, values);	
	}
	
	public boolean delete(ExpenseEntries.Entry entry){
		return delete(entry.getId());
	}
	public boolean delete(int id){
		boolean status = false;
		if(id>-1){
			String[] args = { Integer.toString(id) };
			if(db.delete(ExpenseDbSchema.ExpenseEntries.TABLE_NAME, ExpenseDbSchema.ExpenseEntries.TABLES_ENTRY_ID + " = ", args)>0) status = true;
		}
		return status;
	}
	public boolean update(ExpenseEntries.Entry entry){
		boolean status = false;
		ContentValues values = new ContentValues();
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_ACCOUNT, entry.getAccount());
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_CAT, entry.getCategory());
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_DATE, entry.getDate());
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_DESC, entry.getDescription());
		values.put(ExpenseDbSchema.ExpenseEntries.COLUMN_SUM, entry.getSum());		
		String[] args = { Integer.toString(entry.getId()) };
		int n = db.update(ExpenseDbSchema.ExpenseEntries.TABLE_NAME, values, ExpenseDbSchema.ExpenseEntries.TABLES_ENTRY_ID + " = ", args);
		if(n>0) status = true;
		return status;
	}
}
