package com.matrixd.expenseapp;

import android.provider.BaseColumns;

//class for db scheme
public class ExpenseDbSchema {

	public static abstract class ExpenseIncome implements BaseColumns {
		public static final String TABLE_NAME = "income";
		public static final String TABLES_ENTRY_ID = "id";// (int)
		public static final String COLUMN_SUM = "sum";// (real)
		public static final String COLUMN_DESC = "desc"; //description (text)
		public static final String COLUMN_DATE = "date";// (text)
		public static final String COLUMN_ACCOUNT = "account";// (int) account id
	}

	//table for finance accounts
	public static abstract class ExpenseBank implements BaseColumns {
		public static final String TABLE_NAME = "bank";
		public static final String TABLES_ENTRY_ID = "id";
		public static final String COLUMN_NAME = "name";//account name (text)
		public static final String COLUMN_FUNDS = "funds";//(real)
		public static final String COLUMN_DATE = "date"; //creation date (int) in minutes
		public static final String COLUMN_LAST = "last"; //last change date (int) in minutes
		public static final String COLUMN_CURRENCY = "curr";//(text) currency
	}

	public static abstract class ExpenseEntries implements BaseColumns {
		public static final String TABLE_NAME = "expenses";
		public static final String TABLES_ENTRY_ID = "id";
		public static final String COLUMN_SUM = "sum";//(real)
		public static final String COLUMN_DESC = "desc"; //description (text)
		public static final String COLUMN_DATE = "date";//(int) in minutes
		public static final String COLUMN_CAT = "cat"; //category (int) category id
		public static final String COLUMN_ACCOUNT = "account";//(int) account id
	}
	
	public static abstract class ExpenseSettings implements BaseColumns {
		public static final String TABLE_NAME = "settings";
		public static final String TABLES_ENTRY_ID = "id";
		public static final String COLUMN_FIELD = "field";
		public static final String COLUMN_VALUE = "value";
	}

	private ExpenseDbSchema() {}
	
	public static abstract class ExpenseCat implements BaseColumns {
		public static final String TABLE_NAME = "cats";
		public static final String COLUMN_NAME = "name";//(text)
		public static final String COLUMN_ENTRY_ID = "id";
	}

}
