package com.matrixd.expenseapp;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import com.matrixd.expenseapp.ExpenseDbHelper;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import com.matrixd.expenseapp.FirstLaunch;
import com.matrixd.expenseapp.ExpenseAccounts;

public class MainWindow extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_window);
		ExpenseDbHelper mdbHelper = new ExpenseDbHelper(this);
		SQLiteDatabase db = mdbHelper.getWritableDatabase();
		ExpenseAccounts accounts = new ExpenseAccounts(db);
		List<ExpenseAccounts.Account> lst = accounts.getAll();
		
		if(lst.size() == 0){
			Intent first = new Intent(this, FirstLaunch.class);
			startActivity(first);
		} else {
			
		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_window, menu);
        return true;
    }
}
