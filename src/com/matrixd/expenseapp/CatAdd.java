package com.matrixd.expenseapp;

import android.os.Bundle;
import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.widget.EditText;

public class CatAdd extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cat_add);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_cat_add, menu);
        return true;
    }
    
    public void save(){
		EditText catName = (EditText) findViewById(R.id.catName);
		
		ExpenseDbHelper mdbHelper = new ExpenseDbHelper(this);
		SQLiteDatabase db = mdbHelper.getWritableDatabase();
		
		ExpenseCats cats = new ExpenseCats(db);
		
		ExpenseCats.Cat cat = new ExpenseCats.Cat();
		cat.setName(catName.getText().toString());
		
		cats.insert(cat);
	}
}
