package com.matrixd.expenseapp;

import android.os.Bundle;
import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import com.matrixd.expenseapp.ExpenseAccounts;

public class FirstLaunch extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_launch);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_first_launch, menu);
        return true;
    }
    
    public void save(View view){
    	EditText accName = (EditText) findViewById(R.id.editAcc);
    	EditText cur = (EditText) findViewById(R.id.editCur);
    	EditText initial = (EditText) findViewById(R.id.editInit);
    	ExpenseDbHelper mdbHelper = new ExpenseDbHelper(this);
        SQLiteDatabase db = mdbHelper.getWritableDatabase();
    	ExpenseAccounts accounts = new ExpenseAccounts(db);
    	ExpenseAccounts.Account acc = new ExpenseAccounts.Account();
    	acc.setCurrency(cur.getText().toString());
    	acc.setName(accName.getText().toString());
    	acc.setBalance(Float.valueOf(initial.getText().toString().trim()).floatValue());
    	accounts.insert(acc);
    	
    	db.close();
    }
}
