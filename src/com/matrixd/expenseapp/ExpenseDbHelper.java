package com.matrixd.expenseapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ExpenseDbHelper extends SQLiteOpenHelper {

	// If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ExpenseAppDb.db";
    
    private static final String SQL_CREATE_CATS =
        "CREATE TABLE " + ExpenseDbSchema.ExpenseCat.TABLE_NAME + " (" +
        ExpenseDbSchema.ExpenseCat.COLUMN_ENTRY_ID + " INTEGER PRIMARY KEY," +
        ExpenseDbSchema.ExpenseCat.COLUMN_NAME + " TEXT" + " )";
    
    private static final String SQL_CREATE_INCOME = 
    	"CREATE TABLE " + ExpenseDbSchema.ExpenseIncome.TABLE_NAME + " (" + 
    			ExpenseDbSchema.ExpenseIncome.TABLES_ENTRY_ID + " INTEGER PRIMARY KEY, " +
    			ExpenseDbSchema.ExpenseIncome.COLUMN_ACCOUNT + " INTEGER, " +
    			ExpenseDbSchema.ExpenseIncome.COLUMN_DATE + " INTEGER, " +
    			ExpenseDbSchema.ExpenseIncome.COLUMN_SUM + " REAL " +
    	")";
    
    private static final String SQL_CREATE_EXPENSE =
    		"CREATE TABLE " + ExpenseDbSchema.ExpenseEntries.TABLE_NAME + " (" +
    				ExpenseDbSchema.ExpenseEntries.TABLES_ENTRY_ID + " INTEGER PRIMARY KEY, " +
    				ExpenseDbSchema.ExpenseEntries.COLUMN_SUM + " REAL, " +
    				ExpenseDbSchema.ExpenseEntries.COLUMN_ACCOUNT + " INTEGER, " +
    				ExpenseDbSchema.ExpenseEntries.COLUMN_CAT + " INTEGER, " +
    				ExpenseDbSchema.ExpenseEntries.COLUMN_DESC + " TEXT, " +
    				ExpenseDbSchema.ExpenseEntries.COLUMN_DATE + " INTEGER " +
    		")";
    
    private static final String SQL_CREATE_BANK =
    		"CREATE TABLE " + ExpenseDbSchema.ExpenseBank.TABLE_NAME + " (" +
    				ExpenseDbSchema.ExpenseBank.TABLES_ENTRY_ID + " INTEGER PRIMARY KEY, " +
    				ExpenseDbSchema.ExpenseBank.COLUMN_NAME + " TEXT, " +
    				ExpenseDbSchema.ExpenseBank.COLUMN_FUNDS + " REAL, " +
    				ExpenseDbSchema.ExpenseBank.COLUMN_DATE + " INTEGER, " +
    				ExpenseDbSchema.ExpenseBank.COLUMN_LAST + " INTEGER, " +
    				ExpenseDbSchema.ExpenseBank.COLUMN_CURRENCY + " TEXT " +
    		")";
    
    public ExpenseDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_BANK);
        db.execSQL(SQL_CREATE_EXPENSE);
        db.execSQL(SQL_CREATE_CATS);
        db.execSQL(SQL_CREATE_INCOME);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        String del = "DROP TABLE IF EXISTS ";
    	db.execSQL(del + ExpenseDbSchema.ExpenseBank.TABLE_NAME);
    	db.execSQL(del + ExpenseDbSchema.ExpenseEntries.TABLE_NAME);
    	db.execSQL(del + ExpenseDbSchema.ExpenseIncome.TABLE_NAME);
    	db.execSQL(del + ExpenseDbSchema.ExpenseEntries.TABLE_NAME);
    	db.execSQL(del + ExpenseDbSchema.ExpenseCat.TABLE_NAME);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
