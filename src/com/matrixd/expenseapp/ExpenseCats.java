package com.matrixd.expenseapp;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ExpenseCats {

	static class Cat{
		
		private int id = -1;
		private String name;
		
		private Cat(int idNumber){
			id = idNumber;
		}
		public Cat(){}
		
		public void setName(String category_name){
			name = category_name;
		}
		
		public String getName(){
			return name;
		}
		public int getId(){
			return id;
		}
	}
	
	private SQLiteDatabase db;
	
	public ExpenseCats(SQLiteDatabase database){
		db = database;
	}
	
	public List<Cat> getAll(){
		List<Cat> cats = new ArrayList<Cat>();
		
		Cursor res = db.query(
				ExpenseDbSchema.ExpenseCat.TABLE_NAME,
				null,
				null,
				null,
				null,
				null,
				null
		);
		
		for(int i = 0; i<res.getCount(); i++){
			res.moveToPosition(i);
			Cat cat = new Cat(res.getInt(res.getColumnIndex(ExpenseDbSchema.ExpenseCat.COLUMN_ENTRY_ID)));
			cat.setName(res.getString(res.getColumnIndex(ExpenseDbSchema.ExpenseCat.COLUMN_NAME)));
			cats.add(cat);
		}
		
		return cats;
	}
	
	public Cat get(int id){
		
		String selection = ExpenseDbSchema.ExpenseCat.COLUMN_ENTRY_ID + " = " + id;
		
		Cursor res = db.query(
				ExpenseDbSchema.ExpenseCat.TABLE_NAME,
				null,
				selection,
				null,
				null,
				null,
				null
		);
		
		Cat cat;
		
		if(res.getCount() > 0){
			cat = new Cat(res.getInt(res.getColumnIndex(ExpenseDbSchema.ExpenseCat.COLUMN_ENTRY_ID)));
			cat.setName(res.getString(res.getColumnIndex(ExpenseDbSchema.ExpenseCat.COLUMN_NAME)));
		} else cat = null;
		
		return cat;
	}
	
	public void update(Cat cat){
		ContentValues values = new ContentValues();
		values.put(ExpenseDbSchema.ExpenseCat.COLUMN_NAME, cat.getName());
		
		String where = ExpenseDbSchema.ExpenseCat.COLUMN_ENTRY_ID + " = ";
		String[] args = { Integer.toString(cat.getId()) };
		
		db.update(ExpenseDbSchema.ExpenseCat.TABLE_NAME, values, where, args);
	}
	
	public void insert(Cat cat){
		ContentValues values = new ContentValues();
		values.put(ExpenseDbSchema.ExpenseCat.COLUMN_NAME, cat.getName());
		db.insert(ExpenseDbSchema.ExpenseCat.TABLE_NAME, null, values);
	}
}
