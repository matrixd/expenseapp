package com.matrixd.expenseapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.matrixd.expenseapp.ExpenseDbSchema;

public class Settings {
	
	private SQLiteDatabase db;
	
	public Settings(SQLiteDatabase database){
		db = database;
	}
	
	public String get(String option){
		String selection, orderBy = null, val = null;
		selection = ExpenseDbSchema.ExpenseSettings.COLUMN_FIELD + " = " + option;
		
		Cursor res = db.query(
			ExpenseDbSchema.ExpenseSettings.TABLE_NAME,
			null,
			selection,
			null,
			null,
			null,
			orderBy
		);
		
		if(res.getCount() > 0){
			res.moveToFirst();
			val = res.getString(res.getColumnIndex(ExpenseDbSchema.ExpenseSettings.COLUMN_VALUE));
		}
		return val;
	}
	
	public Setting getSetting(String option){
		String selection, orderBy = null, val = null;
		selection = ExpenseDbSchema.ExpenseSettings.COLUMN_FIELD + " = " + option;
		
		Cursor res = db.query(
			ExpenseDbSchema.ExpenseBank.TABLE_NAME,
			null,
			selection,
			null,
			null,
			null,
			orderBy
		);
		
		if(res.getCount() > 0){
			res.moveToFirst();
			val = res.getString(res.getColumnIndex(ExpenseDbSchema.ExpenseSettings.COLUMN_VALUE));
		}
		Setting s = new Setting(option, val);
		return s;
	}
	
	public void update(Setting setting){
		ContentValues values = new ContentValues();
		values.put(ExpenseDbSchema.ExpenseSettings.COLUMN_VALUE, setting.getValue());
		
		String where = ExpenseDbSchema.ExpenseSettings.COLUMN_FIELD + " = ";
		String[] args = { setting.getName() };
		
		db.update(ExpenseDbSchema.ExpenseSettings.TABLE_NAME, values, where, args);
	}
	
	public class Setting{
		
		private String name, val;
		
		public Setting(String setting_name, String setting_val){
			name = setting_name;
			val = setting_val;
		}
		
		public void setValue(String value){
			val = value;
		}
		
		public String getValue(){
			return val;
		}
		
		public String getName(){
			return name;
		}
	}
	
}
