package com.matrixd.expenseapp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.matrixd.expenseapp.ExpenseDbSchema;

public class ExpenseAccounts {	
	private SQLiteDatabase db;
	
	public ExpenseAccounts(SQLiteDatabase database){
		db = database;
	}
	public Account get(int id){
		String selection, orderBy;
		selection = ExpenseDbSchema.ExpenseBank.TABLES_ENTRY_ID + " = " + id;
		orderBy = null;
		
		Cursor res = db.query(
			ExpenseDbSchema.ExpenseBank.TABLE_NAME,
			null,
			selection,
			null,
			null,
			null,
			orderBy
		);
		
		Account acc = new Account();
		
		if(res.getCount() == 1){ 
			res.moveToFirst();
			acc.setName(res.getString(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_NAME)));
			acc.setCurrency(res.getString(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_CURRENCY)));
			acc.setBalance(res.getFloat(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_FUNDS)));
			acc.setCreationDate(res.getInt(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_DATE)));
			acc.setLastDate(res.getInt(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_LAST)));
		}
		return acc;
	}
	
	public List<Account> getAll(){
		List<Account> accounts = new ArrayList<Account>();
		
		Cursor res = db.query(
				ExpenseDbSchema.ExpenseBank.TABLE_NAME,
				null,
				null,
				null,
				null,
				null,
				null
		);
		
		for(int i = 0; i<res.getCount(); i++){
			res.moveToPosition(i);
			Account acc = new Account(res.getInt(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.TABLES_ENTRY_ID)));
			acc.setName(res.getString(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_NAME)));
			acc.setCurrency(res.getString(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_CURRENCY)));
			acc.setBalance(res.getFloat(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_FUNDS)));
			acc.setCreationDate(res.getInt(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_DATE)));
			acc.setLastDate(res.getInt(res.getColumnIndex(ExpenseDbSchema.ExpenseBank.COLUMN_LAST)));
			accounts.add(acc);
		}
		
		return accounts;
	}
	
	
	public void insert(Account account){ 
		ContentValues values = new ContentValues();
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_CURRENCY, account.getAccountCurrency());
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_NAME, account.getName());
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_DATE, account.getCreationDate());
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_LAST, account.getLastDate());
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_FUNDS, account.getBalance());
		db.insert(ExpenseDbSchema.ExpenseBank.TABLE_NAME, null, values);
	}
	public void update(Account account){
		ContentValues values = new ContentValues();
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_CURRENCY, account.getAccountCurrency());
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_NAME, account.getName());
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_DATE, account.getCreationDate());
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_LAST, account.getLastDate());
		values.put(ExpenseDbSchema.ExpenseBank.COLUMN_FUNDS, account.getBalance());
		
		String where = ExpenseDbSchema.ExpenseBank.TABLES_ENTRY_ID + " = ";
		String[] args = { Integer.toString(account.getId()) };
		
		db.update(ExpenseDbSchema.ExpenseBank.TABLE_NAME, values, where, args);
	}
	
	
	
	static class Account{
		private String name = null, cur = null;
		private float funds = 0;
		private int creationDate = 0, lastDate = 0, idNumber = -1;
		
		public Account(){
			Date date = new Date();
			creationDate = (int) date.getTime()/60000;
		}
		
		private Account(int id){
			idNumber = id;
		}
		
		public String getName(){
			return name;
		}
		public float getBalance(){
			return funds;
		}
		public String getAccountCurrency(){
			return cur;
		}
		public int getCreationDate(){
			return creationDate;
		}
		public int getLastDate(){
			return lastDate;
		}
		public int getId(){
			return idNumber;
		}
		
		
		public void setName(String newName){
			name = newName;
		}
		public void setBalance(float newBalance){
			funds = newBalance;
			Date date = new Date();
			lastDate = (int) date.getTime()/60000;
		}
		public void setCurrency(String newCurrency){
			cur = newCurrency;
		}
		public void setCreationDate(int date){
			creationDate = date;
		}
		public void setLastDate(int date){
			lastDate = date;
		}
	}
}
